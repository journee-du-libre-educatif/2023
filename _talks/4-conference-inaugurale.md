---
name: Conférence inaugurale
speakers:
  - Colin de la Higuera
  - Mélanie Pauly Harquevaux
categories:
  - Conférence
slug: "conference-inaugurale-REL"
long_title: "« Les RELs : toutes les réponses aux questions que vous ne vous étiez jamais posées »"
---

« Les RELs : toutes les réponses aux questions que vous ne vous étiez jamais posées. »

Colin de la Higuera, enseignant chercheur en informatique à l'université Nantes et titulaire de la chaire Unesco RELIA, Mélanie Pauly Harquevaux est coordinatrice de la chaire Unesco RELIA.

La [chaire RELIA](https://chaireunescorelia.univ-nantes.fr/) est le témoignage de l’attention portée par l’UNESCO (Organisation des Nations unies pour l’éducation, la science et la culture) au partage libre des ressources et du savoir.

**Ressource :**
- Le [diaporama](/assets/pdf/les-rel-conference-inaugurale.pdf) qui a servi de support à cette présentation