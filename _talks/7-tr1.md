---
name: "Table ronde 1 : Le libre et la souveraineté numérique"
speakers:
  - Sonia Martin-Abdoulkarim
  - Gael Duval
  - Nicolas Léger
categories:
  - Tables rondes
slug: "table-ronde-1-le-libre-et-la-souverainete-numerique"
---

Table Ronde 1 : Le libre et la souveraineté numérique (Sonia Martin-Abdoulkarim et Gael Duval), animée par Nicolas Léger