---
name: Repas + stands
speakers:
  - Murena
  - Polymny
  - Classe dehors
  - Ceméa
  - Wikimédia France + Vikidia
  - Mixap + Canopé
  - C & F Éditions
  - Dictionnaire des francophones
  - Commown
  - Vittascience
  - Territoire Numérique Éducatif du Finistère
  - Webradio Porte-Voix
categories:
  - Stands
slug: "repas-stands"
---