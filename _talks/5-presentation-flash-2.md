---
name: Présentation flash (Série 2)
speakers:
  - PMB, le logiciel documentaire libre
  - "D.O.R.A. : Apprendre la course d’orientation avec un environnement numérique open-source"
  - "Dé-blocs tes capacités !"
  - "éRUDI : les données ouvertes locales comme matériel pédagogique"
  - "Numérique inclusif responsable et durable : le libre dans le secondaire"
  - mon-oral.net
  - "Ada & Zangemann ou la belle aventure d'une traduction “libre”"
categories:
  - Présentations flash
slug: "presentation-flash-serie-2"
---