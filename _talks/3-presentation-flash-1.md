---
name: Présentation flash (Série 1)
speakers:
  - Un panorama des REL
  - Ecovoit
  - QCMcam
  - Programmation ludique exotique avec Baguette#
  - Parcours M@gistère OpenStreetMap
  - Motminot
  - "MIXAP : applications pédagogiques en réalité augmentée"
categories:
  - Présentations flash
slug: "presentation-flash-serie-1"
---