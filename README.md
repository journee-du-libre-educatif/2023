Ce dossier contient le site de l'édition 2023 de la [Journée du Libre Éducatif 2023](https://journee-du-libre-educatif.forge.apps.education.fr/2023).

La Journée du Libre Éducatif a pour objectif d’acculturer aux communs numériques et de soutenir l’écosystème de celles et ceux qui créent et partagent des logiciels et ressources éducatives libres utiles à la communauté scolaire.

Elle se déroulera de 9h30 à 17h le vendredi 7 avril 2023 au Couvent des Jacobins à Rennes et attend près de 400 participants. C’est la deuxième édition après celle de Lyon en 2022.

Elle est co-organisée par les académies de Rennes et de Nantes et la Direction du numérique pour l’éducation du ministère de l’Éducation nationale et de la Jeunesse.

La journée s’inscrit dans le « soutien au développement des communs numériques » de la Stratégie du numérique pour l’éducation 2023-2027 du ministère.

Ce site a été créé avec l'outil libre de création de site _Jekyll_, et le thème _jekyll-theme-conference_, également sous licence libre.