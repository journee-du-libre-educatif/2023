# Welcome to Jekyll!
#
# This config file is meant for settings that affect your whole blog, values
# which you are expected to set up once and rarely need to edit after that.
# For technical reasons, this file is *NOT* reloaded automatically when you use
# 'jekyll serve'. If you change this file, please restart the server process.

title: Journée du Libre Éducatif 2023
preposition: à la
description: >-
  Deuxième édition de la Journée du Libre Éducatif, vendredi 7 avril 2023 au couvent des Jacobins à Rennes
baseurl: "/2023" # the subpath of your site, e.g. /blog
url: "" # the base hostname & protocol for your site, e.g. https://example.com
gitlab_username:  pages

theme: jekyll-theme-conference

# Build settings
markdown: kramdown
sass:
  style: compressed

# Additional exclude from processing
exclude:
  - .github/
  - README.md
  - LICENSE.md
  - REDISTRIBUTED.md
  - purgecss.config.js

# Collections and their defaults
collections:
  talks:
    output: true
    permalink: ./intervention/:title/
  speakers:
    output: true
    permalink: ./intervenants/:title/
  rooms:
    output: true
    permalink: ./salle/:title/

defaults:
  - scope:
      path: ""
      type: talks
    values:
      layout: talk
  - scope:
      path: ""
      type: speakers
    values:
      layout: speaker
  - scope:
      path: ""
      type: rooms
    values:
      layout: room


conference:

  # Language
  lang: fr

  # Show theme errors:
  show_errors: false

  # Navigation bar
  navigation:
    logo:
      name: Accueil
      # img: 'logo.png'
      url: '/'
    links:
      - name: Programme
        relative_url: /programme/
      # - name: Conférences
       #  relative_url: /talks/
      - name: Intervenants
        relative_url: /intervenants/
      - name: Accès
        relative_url: /lieu/
      - name: Édition précédente
        menu:
          # - name: 2023 (current)
          #  disabled: true
          - name: 2022
            absolute_url: https://drane.ac-lyon.fr/spip/Journee-Du-Libre-Educatif-2022

  # Link Preview for Sharing
  link_preview:
    # also adapt "preposition" property at top for more meaningful descriptions
    disable: false
    img:
      twitter: 'journee-libre-educatif-rennes-opengraph.png?1'      # inside /assets/images/
      open_graph: 'journee-libre-educatif-rennes-opengraph.png'  # inside /assets/images/

  # Main landing page
  main:
    # logo:
      #  img: ''
    links:
      - name: Programme
        relative_url: /programme/
      - name: Intervenants
        relative_url: /intervenants/
      - name: Accès
        relative_url: /lieu/
      - name: Photos
        relative_url: /photos/
      # - name: Tickets
        # disabled: true
        # absolute_url: ''

  # Information boxes
  info_bars:
    - title: Bienvenue à l'édition 2023 de la Journée du Libre Éducatif
      color: primary
      main_only: true
      text: |
        Ce site est hébergé sur la [Forge des Communs Numériques Éducatifs](https://forge.apps.education.fr/journee-du-libre-educatif/journee-du-libre-educatif.forge.apps.education.fr) et construit – bien sûr – à partir de logiciels libres !
    - text: Le code source de ce projet est disponible sur la [Forge des Communs Numériques Éducatifs](URL) !
      color: warning
      pages_only: true

  # Live indications
  live:
    date: 07.04.2023
    timezone: GMT+1
    stop:       240  # in minutes
    streaming:
      enable: false
      pause:     60  # in minutes
      prepend:    5  # in minutes
      extend:     5  # in minutes
    demo:
      enable: false
      duration: 300  # in seconds
      pause:     10  # in seconds

  map:
    # Initial map center point
    home_coord: 48.11519, -1.68188
    # Alternative map providers can be found on https://leaflet-extras.github.io/leaflet-providers/preview/
    # The following do match the Bootstrap design not too badly:
    #   - Hydda.Full
    #   - Thunderforest.Neighbourhood
    #   - Esri.WorldTopoMap
    map_provider: "OpenStreetMap.Mapnik"
    default_zoom: 5

  talks:
    # Talk categories
    main_categories:
      - name: Organisateurs
        color: info
      - name: Présentations flash
        color: success
      - name: Conférence
        color: warning
      - name: Stands
        color: secondary
      - name: Tables rondes
        color: dark
      - name: Ateliers
        color: danger
      - name: Interventions annulées
        color: cancel
        active: false

    # Hide icons on talk overview page
    hide_icons: false

  speakers:
    # Show first and last name
    show_firstname: true

  location:
    # Disable links to locations
    hide: false
    # URL of location / room overview page
    url: '/lieu'
    # Main page title shown on location/room navigation bar
    navbar_title: 'Couvent des Jacobins'
    # Show map on location main page
    map: true

  program:
    # URL of program page
    url: '/programme'

    # Time steps in program table
    time_steps: 15 # in minutes

    # Show intermediary time steps (next to full hours)
    show_alltimes: true