---
layout: home
---

![Intérieur du couvent des Jacobins avec sur un grand écran une affiche de présentation de la Journée du Libre Éducatif](assets/images/journee-libre-educatif-rennes.png)

La Journée du Libre Éducatif a pour objectif d’acculturer aux communs numériques et de soutenir l’écosystème de celles et ceux qui créent et partagent des logiciels et ressources éducatives libres utiles à la communauté scolaire.

Elle s'est déroulée de 9h30 à 17h le vendredi 7 avril 2023 au [Couvent des Jacobins](/lieu/) à Rennes (accueil café de 8h45 à 9h15) et a accueilli 400 participantes et participants. 70 intervenantes et intervenants (venus de toute la France et même de Barcelone, Florence, Copenhague et Tokyo) ont présenté 35 projets et animé 21 ateliers, 14 présentations flash, 10 stands, 4 conférences et 3 tables rondes. C’est la deuxième édition après celle de [Lyon en 2022](https://drane.ac-lyon.fr/spip/Journee-Du-Libre-Educatif-2022).


## Ils en ont parlé !

- [Thierry Noisette sur ZDNet](https://www.zdnet.fr/blogs/l-esprit-libre/libre-et-open-source-express-libre-educatif-april-mozilla-fondation-linux-39956268.htm)
- [Jean-Michel Le Baut dans Le Café pédagogique](https://www.cafepedagogique.net/2023/04/12/le-jour-du-libre-est-arrive/)
- [Un compte rendu collaboratif de la journée](https://codimd.apps.education.fr/s/5wZjw6BPW)

## Organisation

Cette journée a été co-organisée par l’académie de Rennes qui nous a accueilli, l’académie de Nantes et la Direction du numérique pour l’éducation du ministère de l’Éducation nationale et de la Jeunesse.

La Journée du Libre Éducatif s’inscrit dans le plan académique de formation des académies de Rennes et Nantes et dans le cadre du « soutien au développement des communs numériques » de la [Stratégie du numérique pour l’éducation 2023-2027](https://www.education.gouv.fr/strategie-du-numerique-pour-l-education-2023-2027-344263) du ministère.

Tout au long de la journée, les élèves de 1ère de la section accueil du lycée Louis Guilloux étaient présents dans le cadre du chef-d'œuvre pour accueillir les participants. Le livre [Ada & Zangemann](/intervenants/ada-zangemann-traduction-libre/), dont l'histoire a été expliquée lors de la journée, a été distribué gratuitement. Une [série d'entretiens](https://digipad.app/p/388634/247eb30ebbd8d) ont également été enregistrés par la [webradio Porte-Voix](/intervenants/webradio-porte-voix/).

[#JDLE](https://mastodon.mim-libre.fr/tags/JDLE) [#JDLE2023](https://mastodon.mim-libre.fr/tags/JDLE2023)

_Une captation vidéo des interventions en plénière dans La Nef sera mise en ligne ultérieurement._

