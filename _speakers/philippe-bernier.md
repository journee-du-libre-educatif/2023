---
name: Philippe Bernier
first_name: Philippe
last_name: Bernier
---

Philippe Bernier, formateur à la Drane de Rennes, un peu linuxien, a développé des expériences de déploiement de Linux, notamment par souci de sobriété numérique.