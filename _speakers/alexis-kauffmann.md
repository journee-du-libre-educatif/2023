---
name: Alexis Kauffmann
first_name: Alexis
last_name: Kauffmann
function: chef de projet logiciels et ressources éducatives libres DNE
links:
  - name: Page Wikipédia
    absolute_url: https://fr.wikipedia.org/wiki/Alexis_Kauffmann
---

Alexis Kauffmann est chef de projets logiciels et ressources éducatives libres à la Direction du numérique pour l’éducation.