---
name: Clément Grennerat
first_name: Clément
last_name: Grennerat
---

Étudiant en première année à l’INSA Lyon, je suis un adepte du développement de programmes informatiques, répondant aux problèmes de tous les jours. C’est avec passion et ambition que je cherche sans cesse à optimiser notre usage de l’outil numérique.