---
name: Polymny
last_name: Polymny
---

Une présentation par Nicolas Bertrand (PhD), actuellement PDG de la SCIC Polymny Studio.

Présentation et démonstration de l'outil libre et gratuit [Polymny Studio](https://polymny.studio/). 

Polymny studio permet la réalisation, 100 % en ligne, de capsules vidéos depuis chez soi ou bien depuis un studio.

Nous présenterons aussi notre structure ancrée dans l'économie sociale et solidaire.