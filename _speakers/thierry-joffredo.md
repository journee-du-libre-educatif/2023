---
name: Thierry Joffredo
first_name: Thierry
last_name: Joffredo
---

Thierry Joffredo est enseignant en mathématiques, et travaille depuis près de 15 ans aujourd'hui à la Direction des systèmes d'information et de l'innovation, où il occupe aujourd'hui le poste de responsable du pôle Services et outils numériques. Il contribue notamment à l'accompagnement de la mise en œuvre de Toutatice auprès de la communauté éducative bretonne.

Il est également historien des mathématiques, titulaire d'un doctorat en histoire des sciences et des techniques, membre associé de l’Institut de mathématiques de Jussieu-Paris Rive Gauche (CNRS, Sorbonne Université, Université Paris Cité). Membre de l'équipe _ENCCRE_ (Édition Numérique Collaborative et CRitique de l’Encyclopédie), il intervient en tant que contributeur scientifique, mais aussi chargé de la communication auprès du grand public et de la valorisation pédagogique du projet. 