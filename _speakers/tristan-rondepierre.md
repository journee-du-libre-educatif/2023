---
name: Tristan Rondepierre
first_name: Tristan
last_name: Rondepierre
---

Professeur de physique-chimie dans l'académie de Lyon en 1ère STL, terminale générale et BTS Métiers de le Mesure. 

Coordonnateur du réseau national de ressources pour la série STL.