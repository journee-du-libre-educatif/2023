---
name: Rémi Angot
first_name: Rémi
last_name: Angot
---

Professeur de mathématiques en collège. 
Développeur et coordonnateur du site [coopmaths.fr](coopmaths.fr), membre du conseil d'administration de Sésamath et membre de l'ICEM34 (pédagogie de la coopération).
Formateur premier degré à la Faculté d'Éducation de Montpellier.