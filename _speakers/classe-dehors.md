---
name: Classe dehors
last_name: Classe dehors
---

Un programme d’actions et des ressources éducatives libres pour développer la classe dehors.

Présentation par Benjamin Gentils.

Site internet : <https://classe-dehors.org/>

Prochain événement : <https://rencontres-internationales.classe-dehors.org/>
