---
name: Territoire Numérique Éducatif du Finistère
last_name: Territoire Numérique Éducatif du Finistère
ajout_e: true
pluriel: true
---

Une présentation par Mathilde Jégou, cheffe de projet TNE et Élise Héraud, chargée de communication, médiation numérique et valorisation des projets TNE.

TNE est un dispositif de formation et d’équipement adapté aux besoins et aux contextes de chaque territoire, qui cherche à réduire la fracture numérique et accélérer la transformation numérique de l’École.

Site internet : <https://www.ac-rennes.fr/le-finistere-nouveau-territoire-numerique-educatif-123169>