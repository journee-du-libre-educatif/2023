---
name: Simona Levi
first_name: Simona
last_name: Levi
long_title: «&nbsp;Pour une éducation numérique démocratique et ouverte à l’échelle européenne&nbsp;»
links:
  - name: Page wikipédia
    absolute_url: https://fr.wikipedia.org/wiki/Simona_Levi
function: professeure en technopolitique et droits à l’université de Barcelone et fondatrice de Xnet
ajout_e: true
---

« Pour une éducation numérique démocratique et ouverte à l’échelle européenne »

[Simona Levi](https://fr.wikipedia.org/wiki/Simona_Levi) : professeure en technopolitique et droits à l’université de Barcelone, présidente de X-net et co-autrice de la [Déclaration pour une éducation numérique démocratique et ouverte](https://congress.democratic-digitalisation.xnet-x.net/declaration-democratic-digital-education/).