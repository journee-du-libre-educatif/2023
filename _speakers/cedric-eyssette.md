---
name: Cédric Eyssette
first_name: Cédric
last_name: Eyssette
links:
  - name: Page web
    absolute_url: https://eyssette.forge.apps.education.fr/
  - name: Mastodon
    absolute_url: https://scholar.social/@eyssette
  - name: Projets sur la Forge des Communs Numériques Éducatifs
    absolute_url: https://forge.apps.education.fr/eyssette
---

Professeur de philosophie en lycée, je développe des applis, libres et gratuites pour les collègues, et partage des ressources pédagogiques.

[Lien vers le diaporama sur le Markdown](https://eyssette.forge.apps.education.fr/marp-slides/slides/2022-2023/utiliser-le-markdown-pour-tout-faire)