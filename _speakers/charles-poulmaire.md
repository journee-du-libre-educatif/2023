---
name: Charles Poulmaire
first_name: Charles
last_name: Poulmaire
---

Président de l'AEIF (Association des Enseignantes et Enseignants d'Informatique de France).
Agrégé de Mathématiques.
Enseignant 2D en Mathématiques NSI et SNT.
Formateur académique dans l'académie de Versailles.