---
name: "Numérique inclusif responsable et durable : le libre dans le secondaire"
last_name: "Numérique inclusif responsable et durable : le libre dans le secondaire"
pluriel: true
---

Une présentation par Romain Debailleul, et Pascal Beel, enseignants Maths, SIN et NSI.

**Ressource :** 
- Le [diaporama](/assets/pdf/numerique-inclusif-responsable-durable.pdf) qui a servi de support à cette présentation

Le libre en établissement du second degré : présentation d'un ensemble d'actions au lycée
(inclusion des élèves, des professeurs, déploiement, etc.) exclusivement fondées sur le libre.

- Réussites et écueils
- Modalités de mise en place d'un premier déploiement GNU/Linux
- Gestion des relations avec les instances : Rectorat/DRANE, Région, élus
- Partie technique : [https://maths-code.fr/cours/postes-linux-et-kwartz/](https://maths-code.fr/cours/postes-linux-et-kwartz/)
- Retour d'expérience : [https://maths-code.fr/cours/2022/06/28/logiciel-libre-au-lycee-retour-dexperience/](https://maths-code.fr/cours/2022/06/28/logiciel-libre-au-lycee-retour-dexperience/)
