---
name: Katia Béguin
first_name: Katia
last_name: Béguin
function: rectrice de la région académique Pays de la Loire
ajout_e: true 
---

Mme Katia Béguin, Rectrice de la région académique Pays de la Loire.
