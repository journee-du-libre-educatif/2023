---
name: Mathilde Louis
first_name: Mathilde
last_name: Louis
ajout_e: true
---

Chargée de projets pédagogiques chez Wikimédia France. Coordinatrice du jeu Wikeys et du partenariat avec [Vikidia](https://fr.vikidia.org/wiki/Vikidia:Accueil).