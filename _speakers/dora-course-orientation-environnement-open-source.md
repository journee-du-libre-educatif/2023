---
name: "D.O.R.A. : Apprendre la course d’orientation avec un environnement numérique open-source"
last_name: "D.O.R.A. : Apprendre la course d’orientation avec un environnement numérique open-source"
---

Une présentation par [Gaëtan Guironnet](https://i3sp.recherche.parisdescartes.fr/equipe/gaetan-guironnet/), enseignant d’E.P.S. depuis 2015 au collège Gounod de Saint-Cloud (92) et doctorant au sein de l’ED 474 Learning Planet Institute (L.P.I.) et l’institut des sciences du sport santé (URP 3625 - I3SP) de l’université Paris Cité.

**Ressource :** 
- Le [diaporama](/assets/pdf/DORA-course-orientation.pdf) qui a servi de support à cette présentation

D.O.R.A. est un environnement numérique d’apprentissage de la course d’orientation en milieu scolaire. Il est composé 1- d'un capteur GPS par pratiquant, capteur open source basé sur un movuino (développé par Kévin Lhoste au sein du makerlab du L.P.I.) qui envoie les données en wifi sur 2-un serveur autonome Raspberry pi et 3-une interface graphique de traitement et visualisation des données D.O.R.A. open source et sous licence CC développée par Séverin Férard étudiant à l’école 42. Les boitiers sont fabriqués par des imprimantes 3D.

L’intégration des balises sur la carte numérique par les coordonnées GPS permet des interactions avec la trace géolocalisée de l’itinéraire du pratiquant (validation virtuelle de la balise) et connaitre la distance et temps entre balises mais aussi le temps et la distance totaux enregistrés au sein d’un « carnet d’entrainement ». C’est un outil d’exergaming qui permet de se connaitre en acte et de mieux connaitre la nature par un environnement numérique.