---
name: PMB, le logiciel documentaire libre
last_name: PMB, le logiciel documentaire libre
ajout_e: true
---

Une présentation par Edith Boulo, professeure documentaliste au lycée La Pérouse Kérichen, à Brest, et coordinatrice du réseau des accompagnateurs et accompagnatrices PMB de l'académie de Rennes.

**Ressource :** 
- Le [diaporama](/assets/pdf/pmb-logiciel-documentaire-libre.pdf) qui a servi de support à cette présentation

Cet atelier présentera [PMB](https://www.sigb.net/index.php?lvl=cmspage&pageid=6&id_rubrique=1&opac_view=1), le logiciel libre de gestion du fonds documentaire du CDI utilisé par les professeures documentalistes de l'académie de Rennes, hébergé par l'académie et intégré à l'espace numérique de travail Toutatice.

- Historique de l'adoption de PMB et exposé des critères qui ont conduit au choix de ce logiciel libre en 2005, puis à son développement dans le contexte de la construction d'une offre de services pour les établissements qui s'est déployée, au fil des ans, au sein d'un ENT libre. 

- Quelques usages significatifs (construction du portail documentaire, gestion des acquisitions, dépôt d'avis, constitution de listes de lecture...) ou plus spécifiques (base mutualisée, ebooks, créathèque...).

- Présentation du réseau des accompagnateurs et accompagnatrices PMB, dont la mission est de former et assister les professeur⋅es documentalistes de l'académie dans leur usage de PMB, et la manière dont nous collaborons autour des problématiques d'accompagnement de nos collègues et de la définition des évolutions souhaitées sur notre outil commun.

- Les perspectives, nombreuses : évolutions fonctionnelles, améliorations du portail, interrogation des bases mutualisées, et... pourquoi pas, imaginer de faire à terme de PMB un commun numérique qui dépasserait les frontières bretonnes (un peu comme ELEA) ?