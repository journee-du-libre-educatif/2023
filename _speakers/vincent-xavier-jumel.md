---
name: Vincent-Xavier Jumel
first_name: Vincent-Xavier
last_name: Jumel
links:
  - name: Profil
    absolute_url: https://forge.apps.education.fr/vincentxavier
---

Enseignant agrégé de mathématiques et professeur de NSI.
Membre de l'AEIF.