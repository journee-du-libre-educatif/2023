---
name: Mélanie Pauly Harquevaux
first_name: Mélanie
last_name: Pauly Harquevaux
---

Coordinatrice de la Chaire UNESCO RELIA pour les Ressources Éducatives Libres et Intelligence Artificielle.

L’objectif de cette chaire est de développer l’IA au service de l’Éducation à travers la recherche et des projets européens. Ainsi que l’Éducation à l’IA par la formation des enseignants et la création d’actions de médiation scientifique grand public.