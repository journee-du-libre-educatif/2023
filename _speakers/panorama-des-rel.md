---
name: Un panorama des REL
last_name: Un panorama des REL
links:
  # - name: Profile
    # absolute_url: 
forge: https://forge.apps.education.fr/drane-lyon/rel
ajout_e: true
pluriel: true
---

Une présentation par Caroline Guedan, chargée de projets DRANE Site de Lyon, référente académique pour les ressources numériques, et Perrine Douhéret.

**Ressource :** 
- Le [diaporama](/assets/pdf/panorama-des-rel.pdf) qui a servi de support à cette présentation

Présentation d'une REL sur les enjeux, la conception et l'usage des ressources éducatives libres.

Site internet : <https://drane-lyon.forge.apps.education.fr/rel/>