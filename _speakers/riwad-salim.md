---
name: Riwad Salim
first_name: Riwad
last_name: Salim
---

Designer numérique et chargé d'opération sur le projet Urbanités Numériques En Jeux au sein de l'Institut de Recherche et D'Innovation.