---
name: "MIXAP : applications pédagogiques en réalité augmentée"
last_name: "MIXAP : applications pédagogiques en réalité augmentée"
ajout_e: true
---

Une présentation par [Iza Marfisi](https://lium.univ-lemans.fr/team/iza-marfisi/), chercheuse au Laboratoire d'Informatique de l'Université du Mans. Son travail porte sur les méthodes et outils pour que les enseignants puissent, eux même, concevoir leurs propres jeux pédagogiques, adaptés à leurs besoins, et les déployer sur le matériel à disposition (ordinateurs, tablettes, smartphones).

**Ressource :** 
- Le [diaporama](/assets/pdf/mixap.pdf) qui a servi de support à cette présentation

[MIXAP](https://mixap-lium.univ-lemans.fr/) est un projet issu de la recherche du LIUM (Laboratoire d'Informatique de l'Université du Mans) et du CREN (Centre de recherche en enseignement de Nantes) et mené en partenariat avec le Réseau Canopé.

Dans ce projet, un groupe d’enseignants pilotes a contribué à la conception d’une application Open source qui s’appelle MIXAP. Celle-ci permet aux enseignants de créer leurs propres applications pédagogiques, en Réalité Augmentée, sans compétences en programmation informatique. Les enseignants ont notamment identifié les fonctionnalités nécessaires pour créer les scénarios pédagogiques qui leur semblaient utiles, amélioré l’ergonomie et testé l’outil dans leurs classes. L'application s’adapte à tous les champs disciplinaires, de la maternelle au lycée et même en formation professionnelle. Elle permet de créer des activités pédagogiques dynamiques (ex. : ajout de vidéos, de textes ou d'annotations complémentaires sur les pages d'un livre), mais aussi des activités à faire en autonomie (ex. : corrections ou aides sur des fiches d'exercices, correction automatique pour des activités d'associations).

![Affiche d'explication d'un usage de Mixap par Camille Poquet pour faire de l'autocorrection par les élèves et les rendre acteurs de la correction de leur travail](https://mixap-lium.univ-lemans.fr/wp-content/uploads/2023/03/camille-1-1.png)

![Affiche d'explication d'un usage de Mixap par Tony Neveu pour proposer des aides orales et de l'autoévaluation tout au long d'une tâche](https://mixap-lium.univ-lemans.fr/wp-content/uploads/2023/03/tony-1-1.png)

![Affiche d'explication d'un usage de Mixap par Delphine Deshayes pour créer une activité avec un album jeunesse afin de renforcer le lexique, l'écriture et comparer les illustrations et les objets réels](https://mixap-lium.univ-lemans.fr/wp-content/uploads/2023/03/TemplatePosterLIUM-1-1.png)