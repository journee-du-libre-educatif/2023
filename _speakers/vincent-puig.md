---
name: Vincent Puig
first_name: Vincent
last_name: Puig
---

Vincent Puig est directeur de l'Institut de Recherche et d'Innovation qu'il a créé avec le philosophe Bernard Stiegler. Il travaille actuellement sur l'organologie numérique des savoirs et notamment sur le geste et le jeu. 