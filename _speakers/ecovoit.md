---
name: Ecovoit
last_name: Ecovoit
---

Une présentation par Philippe Logiou, enseignant en BTS SIO au Lycée Rabelais de Saint-Brieuc.

**Ressource :** 
- Le [diaporama](/assets/pdf/ecovoit.pdf) qui a servi de support à cette présentation

Sur une idée des éco-délégués, nous avons souhaité mettre en place un site de co-voiturage dédié aux seuls élèves et personnels du lycée. 

J'ai accompagné Bastien ROUX, un de mes étudiants de BTS SIO, au développement d'une application répondant à cette vision. 

L'objet est de présenter les différentes étapes par lesquelles nous sommes passées, de la conception au rapprochement avec la DSI de l'académie pour l'intégration dans l'ENT Toutatice, et d'aborder les différentes questions que nous nous sommes posées (conformité au RGPD, ouverture du code, constitution de la base de participants, sécurisation de l'application, intégration à l'écosystème de l'ENT, ...).

Nous nous inscrivons dans une première étape d'usage pour notre établissement avec comme cible l'ouverture à toute l'académie dans un second temps.

Le code est destiné à être disponible pour tous sur la forge de la DSI et référencé sur code.gouv.fr.