---
name: QCMcam
last_name: QCMcam
forge: https://forge.apps.education.fr/sebastien.cogez/qcmcam
---

Une présentation par Sébastien Cogez, professeur de mathématiques 2D en collège dans les Pyrénées Orientales.

**Ressource :** 
- Le [diaporama](/assets/pdf/qcm-cam.pdf) qui a servi de support à cette présentation

QCMcam est un outil permettant de faire des QCMs auxquels les participants répondent à l'aide d'un carton scanné par une webcam.

Site web : [https://qcmcam.net](https://qcmcam.net)

Le code source de cet outil est libre et disponible sur la Forge des Communs Numériques Éducatifs : [https://forge.apps.education.fr/sebastien.cogez/qcmcam](https://forge.apps.education.fr/sebastien.cogez/qcmcam)


Sébastien Cogez a également participé au développement d'outils libres et gratuits pour Sésamath : mathenpoche réseau, kidimath, labomep.
Depuis 2013, il développe aussi un outil permettant de créer très rapidement des diaporamas corrigés à données aléatoires pour travailler les automatismes en mathématiques : MathsMentales [https://mathsmentales.net](https://mathsmentales.net).