---
name: Commown
last_name: Commown
---

Des appareils plus éthiques et durables. La coopérative [Commown](https://commown.coop/) propose de faire durer le matériel informatique grâce à la location longue durée.

Une présentation par Florent Cayré, cofondateur.