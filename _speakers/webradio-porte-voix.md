---
name: Webradio Porte-Voix
last_name: Webradio Porte-Voix
---


La [webradio Porte-Voix](https://culturesnumeriques.ac-rennes.fr/spip.php?article183), c’est une webradio qui porte la voix des élèves de la région académique Bretagne, met en réseau la centaine de webradios présente sur le territoire breton et les radios partenaires qui les soutiennent, pour développer l’esprit critique, la curiosité, distinguer le vrai du faux, argumenter, débattre et enrichir les compétences transversales, orales et écrites pour faire-savoir.

Une sélection d’une heure de podcasts quotidienne sera mise en ligne sur le site [Interactik / Porte-Voix](https://www.interactik.fr/portail/web/radio-porte-voix2).

Par ailleurs, pour offrir un service à tous les créateurs de contenus radiophoniques, Porte-Voix s’accompagne d’un projet de mise en place d’une sonothèque collaborative libre, alimentée par les élèves sur le mode d’une “Pêche aux sons” ludique et pédagogique.