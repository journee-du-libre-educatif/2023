---
name: Olivier Ertzscheid
first_name: Olivier
last_name: Ertzscheid
function: maître de conférences en sciences de l'information et de la communication et blogueur
links:
  - name: Page Wikipédia
    absolute_url: https://fr.wikipedia.org/wiki/Olivier_Ertzscheid
  - name: "Son blog: Affordance.info"
    absolute_url: https://affordance.framasoft.org/
---

Olivier Ertzscheid, né en 1972, est un chercheur français en sciences de l'information et de la communication, qui enseigne en tant que maître de conférences à l'université de Nantes et à l'Institut universitaire de technologie de La Roche-sur-Yon. Il s'intéresse principalement à l'évolution des dispositifs et des usages numériques, en particulier dans le domaine de la culture, des métiers du livre et de la documentation. Il publie régulièrement dans les médias qui traitent de ces questions ainsi que sur son blog.

**Ressource :** 
- Le [diaporama](/assets/pdf/un-monde-de-migration-s.pdf) qui a servi de support pour la conférence inaugurale