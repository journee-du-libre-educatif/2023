---
name: Programmation ludique exotique avec Baguette#
last_name: Programmation ludique exotique avec Baguette#
ajout_e: true
---

Une présentation par Charlotte Thomas, étudiante en L2 post prépa, informatique théorique, amatrice de langage formels et de OCaml et chercheuse stagiaire à l'IRISA/IRISA Rennes sur l'optimisation de gcode pour l'impression 3D.

**Ressource :** 
- Le [diaporama](/assets/pdf/programmation-ludique-exotique-baguette.pdf) qui a servi de support à cette présentation

[Le baguette# ](https://github.com/coco33920/baguette-sharp)est un langage exotique que j'ai créé où l'on code avec des pâtisseries ! Par exemple pour afficher un _hello world_ c'est

"CROISSANT CHOUQUETTE PARISBREST Hello World CLAFOUTIS BAGUETTE"

[L'interpréteur](https://github.com/coco33920/ocaml-baguettesharp-interpreter) est en OCaml et il a même son propre _package manager_ ! [BOULANGERIE : Baguette# Package Manager](https://github.com/coco33920/boulangerie).

Il a été créé pour les TIPE de prépa mais continue de s'étendre et je le trouve très ludique. Tout le code du langage ainsi que du package manager sont disponibles en licence GPL-3 ! 

Quelques chercheurs du milieu ont participé soit en mettant une issue soit en me donnant des pointeurs par message :)