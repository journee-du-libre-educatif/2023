---
name: Emmanuel Zimmert
first_name: Emmanuel
last_name: Zimmert
---

Évoluant conjointement dans les domaines de l'enseignement et de l'apprentissage du français, de la promotion de la langue française dans le monde et du numérique pour l'éducation depuis près de 15 ans, j'ai occupé différents postes de coopération à l'étranger (au Yémen, en Indonésie, au Laos, en Inde et aujourd'hui au Danemark). 

En 2020, j'ai lancé le projet La Digitale, une collection d'outils libres pour l'éducation, avec pour objectif de proposer une vision et un modèle alternatifs qui ne sont pas guidés par la course au profit et la collecte de données.

[https://ladigitale.dev/emmanuel-zimmert.html](https://ladigitale.dev/emmanuel-zimmert.html)