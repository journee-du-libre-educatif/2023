---
name: Mixap + Canopé
last_name: Mixap + Canopé
pluriel: true
---

Une présentation par Iza Marfisi, Benoît Challemel du Rozier, Maud Daré et Solenn Malgorn

## Canopé : présentation de "Fab Lab à l'école"

Fab Lab à l’école est un dispositif déployé par Réseau Canopé en partenariat avec Universcience permettant, en milieu scolaire, la diffusion de la culture de fabrication numérique. Il consiste à proposer aux établissements scolaires un mini-Fab Lab, un accompagnement et une formation ainsi que plusieurs outils pédagogiques.

Un mini Fab Lab dans une école primaire :

* 5 machines de fabrication (3 numériques, 2 non numériques)
* 21 tutoriels vidéo sur l’utilisation des machines
* Des exemples de projets de fabrication en lien avec les programmes
* Des outils en ligne pour échanger, partager et documenter

## Mixap

[MIXAP](https://mixap-lium.univ-lemans.fr/) est un projet issu de la recherche du LIUM (Laboratoire d'Informatique de l'Université du Mans) et du CREN (Centre de recherche en enseignement de Nantes) et mené en partenariat avec le Réseau Canopé.

Dans ce projet, un groupe d’enseignants pilotes a contribué à la conception d’une application Open source qui s’appelle MIXAP. Celle-ci permet aux enseignants de créer leurs propres applications pédagogiques, en Réalité Augmentée, sans compétences en programmation informatique. Les enseignants ont notamment identifié les fonctionnalités nécessaires pour créer les scénarios pédagogiques qui leur semblaient utiles, amélioré l’ergonomie et testé l’outil dans leurs classes. L'application s’adapte à tous les champs disciplinaires, de la maternelle au lycée et même en formation professionnelle. Elle permet de créer des activités pédagogiques dynamiques (ex. : ajout de vidéos, de textes ou d'annotations complémentaires sur les pages d'un livre), mais aussi des activités à faire en autonomie (ex. : corrections ou aides sur des fiches d'exercices, correction automatique pour des activités d'associations).

Lors de l'atelier, vous pourrez tester MIXAP. Amenez un smartphone ou une tablette de votre établissement et quelques éléments de votre matériel pédagogique (livre, images, cartes, fiches d'exercices). Mais si vous êtes juste curieux, venez sans rien, nous vous prêtons tout ce qu'il faut !

![Logo de Mixap au centre avec une femme qui tient une tablette dans ses mains qui affiche de la réalité augmentée](https://mixap-lium.univ-lemans.fr/wp-content/uploads/2023/03/mixap-obj-1-1536x1215.png)