---
name: Michel Briand
first_name: Michel
last_name: Briand
---

Acteur des réseaux coopératifs et des communs, co-anime les sites contributifs a-brest, Bretagne Créative, Bretagne-Educative.net, Innovation-pédagogique et organisateur du Forum des usages Coopératifs (initié en 2004); 
- et en cette période de crise les espaces collaboratifs : Riposte Créative Territoriale, Pédagogique, et Bretagne 
- acteur des communs tels Brest en communs (initié en 2009) et publie sur le blog “Coopérations” et Histoires de Coopérations 
- élu à Brest (1995-2014) a développé une politique publique appropriation sociale du numérique 
- et co auteur du rapport du Conseil National du Numérique : « Citoyens d’une société numérique, Accès, Littératie, Médiations, Pouvoir d’agir » (octobre 2013).