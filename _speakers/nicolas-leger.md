---
name: Nicolas Léger
first_name: Nicolas
last_name: Léger
---

Professeur de philosophie au lycée français de Florence du réseau _Mlfmonde_ et auteur à la revue _Esprit_.