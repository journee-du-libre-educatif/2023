---
name: Apollonie Raffalli
first_name: Apollonie
last_name: Raffalli
ajout_e: true
---


J’enseigne l’informatique au sein du BTS SIO (services informatiques aux organisations).

Depuis ma nomination en 1994 en tant que professeure, je gère l’équipement informatique du lycée en privilégiant les outils libres avec notamment le déploiement d'un environnement numérique de travail local (SambaEdu) en 2005.

Tout au long de ma carrière, j'ai été chargée de la mise en place et de l'animation de formations en TICE (technologies de l'information et de la communication pour l'enseignement).

J'ai également occupé la fonction d'Interlocuteur Académique en économie-gestion pendant 10 ans et je suis actuellement référente numérique au sein de mon établissement.

Je suis depuis toujours impliquée dans de nombreux projets de développement d'outils (le dernier en date concerne le développement de l’e-comBox) et de ressources pédagogiques notamment au sein du réseau Certa.