---
name: Jean-Michel Le Baut
first_name: Jean-Michel
last_name: Le Baut
---

Professeur de français à Brest, formateur DRANE académie de Rennes, ambassadeur “Citoyennetés numériques” du _Living Lab Interactik_.