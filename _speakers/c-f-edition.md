---
name: C & F Éditions
last_name: C & F Éditions
pluriel: true
---

[C & F Éditions](https://cfeditions.com/public/) est une maison d’édition française créée à Caen (France) en 2003. Le projet éditorial est centré sur la culture numérique et sa critique.

Présentation par Hervé Le Crosnier et Yann Trividic.