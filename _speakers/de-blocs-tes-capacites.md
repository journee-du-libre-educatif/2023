---
name: "Dé-blocs tes capacités !"
last_name: "Dé-blocs tes capacités !"
---

Une présentation par Sébastien Canet, enseignant de Technologie au collège, de Sciences de l’Ingénieur en Lycée et d’électronique en BTS, formateur et animateur d'ateliers autour des pratiques & outils numériques.

**Ressource :** 
- Le [diaporama](/assets/pdf/des-blocs-toi.pdf) qui a servi de support à cette présentation

Découvrez les cartes à microcontrôleurs (Arduino et son univers, micro:bit, Picaxe, etc) et les moyens graphiques pour les programmer. Comment ces logiciels sont utilisés dans les écoles/collèges/lycées ? Mais aussi quels sont leurs atouts et limites pour une vraie initiation à la programmation ?

[https://github.com/technologiescollege](https://github.com/technologiescollege)