---
name: Murena
last_name: Murena
pluriel: true
---

Une présentation par Gaël Duval et Alexis Noetinger.

[Murena](https://murena.com/fr/) conçoit des smartphones et des services cloud qui donnent la priorité à la vie privée des utilisateurs. Toute la gamme des produits proposés se fonde sur des logiciels libres, notamment le système d'exploitation déGooglisé /e/OS et NextCloud.