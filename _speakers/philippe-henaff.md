---
name: Philippe Hénaff
first_name: Philippe
last_name: Hénaff
---

Nouveau retraité, ERUN du Finistère pendant 20 ans
Conception et développement du BNE29 pour Windows (V 2.8) et du BNE version Linux.
Co-conception avec Franck Rouillé et nouveau développement de LibreOffice des écoles.
Conception et de développement de Scolbuntu 12.4 et 14.2 (distribution Linux maintenant obsolète)