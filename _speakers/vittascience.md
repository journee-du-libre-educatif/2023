---
name: Vittascience
last_name: Vittascience
pluriel: true
---

[Vittascience](https://fr.vittascience.com/) est une plateforme éducative pensée pour l'apprentissage du codage, qui propose des outils innovants pour l'enseignement.

Présentation par Damien Vallot et Léo Briand.