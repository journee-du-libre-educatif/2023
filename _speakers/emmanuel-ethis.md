---
name: Emmanuel Ethis
first_name: Emmanuel
last_name: Ethis
function: recteur de la région académique Bretagne
---

M. Emmanuel Ethis, Recteur de la région académique Bretagne