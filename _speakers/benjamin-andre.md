---
name: Benjamin André
first_name: Benjamin
last_name: André
---

Je suis co-fondateur de Cozy Cloud, une startup dont l'histoire démarre par un simple constat : une seule personne peut réunir plus de donnée que Google : l'utilisateur ! Cozy est le premier domicile numérique, un espace de stockage intelligent qui permet à l'utilisateur de récupérer automatiquement TOUTES ses informations et d'ainsi bénéficier de services et d'une intégration qui dépasse celles des silos fermés des GAFA. 

L’application à l’éducation est particulièrement naturel et puissant. L’Espace Numérique Personnel de l’élève devient son « cartable numérique » qui le suit tout au long de sa scolarité pour y collaborer avec ses enseignants et camarades. Mais aussi y capitaliser toutes ses traces pédagogiques pour rendre possibles des services d’orientation et d’enseignement personnalisés, impossibles quand les données sont dispersées dans des silos étanches, dans des formats hétérogènes.

L’Espace Numérique Personnel est un levier d’émancipation numérique de l’élève, et au-delà, un outil au service de la pertinence de son parcours scolaire. L'ENPE est développé avec l'académie de Rennes notamment dans le cadre d'un Challenge Éducation "MyToutatice".