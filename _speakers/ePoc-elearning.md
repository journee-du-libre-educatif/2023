---
name: ePoc, application mobile libre de e-learning
last_name: ePoc, application mobile libre de e-learning
---

Une présentation par Jean-Marc Hasenfratz.

ePoc [electronic Pocket open course] est une application mobile gratuite et open source de formation au numérique développée par Inria Learning Lab. 

L'objectif : proposer des formations au numérique à portée de main.
Chaque formation ePoc est développée :
- en assurant sa qualité scientifique : les contenus sont élaborés en collaboration avec des chercheurs spécialistes ou experts du domaine ;
- en respectant la vie privée : aucune collecte de données personnelles et aucun compte n’est à créer ;
- en proposant une attestation de réussite, qui peut être téléchargée à la fin de la formation.

L'application est disponible gratuitement et accessible en open Source (Téléchargement : [https://epoc.inria.fr](https://epoc.inria.fr)). 

Actuellement, 4 ePoc sont disponibles gratuitement (entre 1 et 2h de formation chacun) avec des parcours pédagogiques engageants et spécialement conçus pour le mobile :
- B.A-BA des data : introduire les fondamentaux indispensables relatifs aux données à travers des activités simples et variées
- Smartphone et planète : identifier les impacts du smartphone sur l’environnement grâce à 3 scénarios illustrés : Serial Casseur, Autopsie d’un smartphone, La tête dans les nuages.
- Vie Privée et smartphone : découvrir l’écosystème des applications et leur usage des données personnelles
- Internet des objets et vie privée : comprendre les implications liées à l’usage d’objets connectés dans la maison dite intelligente.

Le code de l'application est accessible depuis : [https://gitlab.inria.fr/learninglab/epoc/epoc-mobile](https://gitlab.inria.fr/learninglab/epoc/epoc-mobile)
Le code de l'éditeur qui permet de construire chaque ePoc est accessible depuis : [https://gitlab.inria.fr/learninglab/epoc/epoc-editor](https://gitlab.inria.fr/learninglab/epoc/epoc-editor) 

Nous cherchons à construire une communauté pour le développement de l'application et de l'éditeur mais aussi une communauté de producteurs de contenus.