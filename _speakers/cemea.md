---
name: Ceméa
last_name: Ceméa
pluriel: true
---

Présentation par Laurent Costy et David Bellanger.

Les Ceméa (Centre d'entraînement aux méthodes d'éducation actives) sont une association d'Éducation Populaire et un Mouvement d'éducation Nouvelle en 1937.

Souvent connus pour les formations d'animateurs/ trices et directeurs/trices de colonies de vacances, les Ceméa, en lien étroit avec l'éducation de part la nature de ses membres, se sont très vite préoccupés des questions de logiciels libres, de numérique éthique et de communs numériques.

Ils sont à l'origine entre autre de Zourit (https://zourint.net), sorte d'AMAP du numérique dont l'objet est de faciliter la collaboration au sein des petites et moyennes associations. Ils forment aussi différents publics aux enjeux des communs numériques et du numérique éthique alternatif.

Site internet : <https://www.cemea.asso.fr/>