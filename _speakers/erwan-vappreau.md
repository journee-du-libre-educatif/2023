---
name: Erwan Vappreau
first_name: Erwan
last_name: Vappreau
---

Enseignant en 1D, impliqué dans diverses dynamiques associatives traitant de la création numérique à l’école et déploiement de pédagogies d projets ([www.tierslieuxedu.org](www.tierslieuxedu.org) / coalition des communs pédagogiques [fabpeda.org](fabpeda.org) / planète Sciences …).

Engagé dans la mise en œuvre d’événements soutenant les échanges de pratiques (ex : « Je fabrique mon matériel pédagogique » ) et de formations en création numérique ou en culture scientifique).

Utilisateur et contributeur au développement du programme [do•doc](https://latelier-des-chercheurs.fr/outils/dodoc) depuis 2018.