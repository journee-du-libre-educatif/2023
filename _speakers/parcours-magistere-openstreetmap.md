---
name: Parcours M@gistère OpenStreetMap
last_name: Parcours M@gistère OpenStreetMap
---

Une présentation par Cédric Frayssinet, chargé de mission DRANE Lyon et Enseignant

Conception d'un parcours M@gistère sur OpenStreetMap.

**Ressource :** 
- Le [diaporama](/assets/pdf/parcours-magistere-openstreetmap.pdf) qui a servi de support à cette présentation