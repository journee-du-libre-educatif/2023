---
name: Yannick Bré
first_name: Yannick
last_name: Bré
---

DSII - Rectorat de l'académie de Rennes.
Référent fonctionnel services numériques Toutatice (pédagogie, collaboration) et numérique en établissement.
Officier SSI (Sécurité des systèmes d’information).
Product Owner du projet MyToutatice.