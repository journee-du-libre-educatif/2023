---
name: Annabel Bourdé
first_name: Annabel
last_name: Bourdé
ajout_e: true
---

DSII - Rectorat de l'académie de Rennes.
Product Owner Toutatice, Cheffe de projet MyToutatice et Référente académique DAC (Données, algorithmes et codes sources).