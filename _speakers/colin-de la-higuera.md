---
name: Colin de la Higuera
first_name: Colin
last_name: de la Higuera
---

Intervenant en recherche et en formation sur les liens entre éducation ouverte et Ressources Éducatives Libres d'une part, Intelligence artificielle d'autre.