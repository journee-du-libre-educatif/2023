---
name: L.L. de Mars
first_name: L.L.
last_name: de Mars
---

Travail artistique pluridisciplinaire depuis le milieu des années 80. 

Bibliographie complète ici : [http://www.le-terrier.net/biblio_lldm/](http://www.le-terrier.net/biblio_lldm/)