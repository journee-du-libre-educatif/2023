---
name: Motminot
last_name: Motminot
forge: https://forge.apps.education.fr/educajou/motminot
---

Motminot : une présentation par Arnaud Champollion, professeur des Écoles (1D) et
ERUN circonscription de Digne-les-Bains (Alpes de Haute Provence).

**Ressource :** 
- Le [diaporama](/assets/pdf/motminot.pdf) qui a servi de support à cette présentation

[Motminot](http://www.circo-digne.ac-aix-marseille.fr/applis/motminot2/) est un jeu inspiré de l'ancien jeu télévisé "Motus", adapté pour les élèves d'élémentaire. En six essais il faut deviner un mot. On ne peut proposer que des mots du dictionnaire, accords et conjugaisons comprises. Après chaque proposition, Motminot indiquera les lettres correctes et le cas échéant si elles sont bien placées, façon "Mastermind".

C'est une adaptation scolaire de [https://motchus.fr/](https://motchus.fr/), qui est lui-même une reprise en VO marseillaise de [https://sutom.nocle.fr/](https://sutom.nocle.fr/) , à son tour basé sur Wordle. Très bon exemple de ce que sont les "forks" dans le logiciel libre et ce que cela permet.

Le lexique standard est issu du de Grammalecte.

Le code de Motminot est hébergé sur [https://forge.apps.education.fr/educajou/motminot](https://forge.apps.education.fr/educajou/motminot)

En outre la Circonscription de Digne, dont je suis le webmaster, en héberge deux instances destinées aux cycles 2 et 3 : [http://www.circo-digne.ac-aix-marseille.fr/applis/](http://www.circo-digne.ac-aix-marseille.fr/applis/)

À partir de cette instance, les classes particpantes peuvent échanger des commentaires sur un Digipad associé (actuellement peu actif car vacances) : [https://digipad.app/p/170419/42707071867f3](https://digipad.app/p/170419/42707071867f3)

Je peux faire une présentation rapide de l'application en conférence, puis animer un atelier pour la prise en main (fonctionnement d'une instance, personnalisation des mots ...).