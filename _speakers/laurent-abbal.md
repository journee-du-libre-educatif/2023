---
name: Laurent Abbal
first_name: Laurent
last_name: Abbal
---

Enseignant de Physique-Chimie et Numérique et Sciences Informatiques au Lycée Français International de Tokyo (établissement AEFE).