---
name: Alexandre Guilbaud
first_name: Alexandre
last_name: Guilbaud
---

Alexandre Guilbaud est maître de conférences à Sorbonne Université, membre de l’Institut de mathématiques de Jussieu-Paris Rive Gauche (CNRS, Sorbonne Université, Université Paris Cité).

Ses recherches portent sur l’histoire des sciences mathématiques et physico-mathématiques et de leurs interactions au XVIIIe siècle, ainsi que sur l’_Encyclopédie ou Dictionnaire raisonné des sciences, des arts et des métiers_ de Diderot et D’Alembert (1751-1772) ; une partie s’inscrit également dans le domaine des humanités numériques.

Il dirige l’_Édition Numérique Collaborative et CRitique de l’Encyclopédie_ (ENCCRE) de Diderot et D’Alembert et participe à l’édition des _Œuvres complètes_ de D’Alembert (1717-1783).

Il enseigne l’histoire des mathématiques et l’histoire des sciences et des savoirs.