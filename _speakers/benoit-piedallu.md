---
name: Benoît Piédallu
first_name: Benoît
last_name: Piédallu
---

Chef de projet des Services Numériques Partagés, au sein du bureau Socle 2 de la DNE.