---
name: Laurence Farhi
first_name: Laurence
last_name: Farhi
ajout_e: true
---

Ingénieure pédagogique dans l'équipe Inria Learning Lab, j'accompagne des chercheurs dans la conception et la réalisation de cours en ligne (Moocs sur la plateforme FUN ou applications sur mobile), dans le domaine des sciences du numérique.