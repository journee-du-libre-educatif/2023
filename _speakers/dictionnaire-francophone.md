---
name: Dictionnaire des francophones
last_name: Dictionnaire des francophones
---

Une présentation par Noé Gasparini, directeur d’édition du dictionnaire des francophones, 
Julie Vasse, enseignante et formatrice, et Sébastien Gathier, responsable des données.

Le [Dictionnaire des francophones](https://www.dictionnairedesfrancophones.org/) est un dictionnaire numérique et collaboratif pour illustrer et faire vivre la richesse du français dans le monde.