---
name: "Ada & Zangemann ou la belle aventure d'une traduction “libre”"
last_name: "Ada & Zangemann ou la belle aventure d'une traduction “libre”"
forge: https://forge.apps.education.fr/nicolas-taffin-ext/ada-lelivre
ajout_e: true
pluriel: true
---

Une présentation par Marion Gaudry + Annaik Guillome

**Ressource :** 
- Le [diaporama](/assets/pdf/ada-zangemann.pdf) qui a servi de support à cette présentation

Projet de traduction collaborative par des élèves du livre [Ada & Zangemann](https://fsfe.org/activities/childrensbook/) d'origine allemande.

Ce conte moderne a pour héroïne une petite fille débrouillarde et pleine de ressources qui, en s’opposant au célèbre inventeur Zangemann, réalise à quel point il est primordial pour elle et pour les autres de pouvoir agir sur la technologie.

Il a été écrit par Matthias Kirschner et est illustré par Sandra Brandstätter.

Matthias Kirschner étant par ailleurs président de la [FSFE](https://fsfe.org/) (Free Software Foundation Europe), le livre a été placé sous licence libre (Creative Commons By-Sa) afin d'en faciliter la diffusion et, justement, ses traductions.

Sur une idée d'Alexis Kauffmann et coordonné par l'ADEAF (Association pour le Développement de l’Enseignement de l’Allemand en France), le livre a été traduit de novembre 2022 à février 2023 par plus d'une centaine d'élèves de quatre établissements scolaires d'Alès, Besançon, Guingamp et Paris dans le cadre d'activités pédagogiques en classe avec leurs professeurs.

Ils se sont partagés le travail en utilisant l'outil libre Digipad de La Digitale ([pour en savoir plus](https://adeaf.net/Ada-Zangemann-traduction-collaborative-d-un-conte-moderne)).

Le dépôt officiel de la traduction se trouve [sur la forge de la FSFE](https://git.fsfe.org/FSFE/ada-zangemann).

La version texte brut actualisée de la traduction se trouve [ici](https://git.fsfe.org/FSFE/ada-zangemann/src/branch/main/Ada_Zangemann-fr.txt).

On trouvera également une [version texte](https://git.fsfe.org/FSFE/ada-zangemann/src/branch/main/Presentations/fr/ada-zangemann-reading.fr.txt) optimisée pour la lecture et la présentation publique accompagnée de [slides](https://git.fsfe.org/FSFE/ada-zangemann/src/branch/main/Presentations/fr/ada-zangemann-reading.fr.pdf) sans texte comportant uniquement les illustrations. Ceci permet à professeurs et élèves de proposer lectures en classe et enregistrements audio s'ils le souhaitent.