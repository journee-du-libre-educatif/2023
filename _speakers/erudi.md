---
name: "éRUDI : les données ouvertes locales comme matériel pédagogique"
last_name: "éRUDI : les données ouvertes locales comme matériel pédagogique"
pluriel: true
---

Une présentation par Gwenola Dubois, formatrice territorial Bretagne - Pays de la Loire Canopé et Guillaume Viniacourt.

**Ressource :** 
- Le [diaporama](/assets/pdf/erudi.pdf) qui a servi de support à cette présentation

[éRudi](https://www.reseau-canope.fr/erudi.html) est un projet Co-porté par Réseau Canopé, l'académie de Rennes, les petits débrouillards et le GTnum1 - Litteratie des données en partenariat avec Rennes Métropole pour profiter de la mise en place d'une nouvelle plateforme d'open data (financement européen) pour engager les enseignants et les élèves du territoires sur le développement d'une nouvelle forme de citoyenneté : s'emparer des données publiques contextualisées pour des projets pédagogiques.

À travers des formations, ateliers, conférences et défi pédagogique, les partenaires ont proposé des interfaces de médiation pour une éducation à la donnée. Plusieurs scénarii pédagogiques ont été créés et partagés. L'atelier vous proposera une brève présentation des potentiels de la donnée comme matériel pédagogique pour vous engager dans un scénario de la recherche de données à la visualisation.