---
name: mon-oral.net
last_name: mon-oral.net, une plateforme libre pour la pratique de l’oral et l’entraînement aux examens
forge: https://forge.apps.education.fr/mon-oral
---

Une présentation par Laurent Abbal.

**Ressource :** 
- Le [diaporama](/assets/pdf/mon-oral-net.pdf) qui a servi de support à cette présentation

[mon-oral.net](https://www.mon-oral.net) est une plateforme libre, conçue par des enseignants, qui permet de développer la pratique de l'oral au primaire et au secondaire, de préparer les élèves aux épreuves orales de collège et de lycée et de créer des commentaires audio pour les élèves.

* Site : [https://www.mon-oral.net](https://www.mon-oral.net)
* Dépôt : [https://forge.apps.education.fr/mon-oral/www.mon-oral.net](https://forge.apps.education.fr/mon-oral/www.mon-oral.net)

Il y a actuellement 4500 enseignants inscrits. Plus de 10000 sujets et 150000 enregistrements ont été créés.

Dernières statistiques (par semaine) : une centaine d'inscriptions et environ 8000 enregistrements

Avec mon-oral.net, les enseignants peuvent créer et proposer aux élèves des activités (récitation, lecture expressive, explication linéaire, description d'image / schéma / graphique, exposé, podcast...) ou des entraînements (avec temps de préparation, tirage au sort de sujet et chronométrage) pour le brevet, l'épreuve anticipée de francais, le Grand Oral, les épreuves de langues... Les enseignants récupèrent automatiquement les enregistrements des élèves dans leur interface d'administration et peuvent via cette même interface corriger et commenter les travaux des élèves.

Les enseignants peuvent aussi créer des capsules audio pour les élèves: correction orale de copies, cours, consignes, explications...

Pour utiliser mon-oral.net, les élèves n'ont pas besoin de créer de compte, ils utilisent un code fourni par l'enseignant. Tout se fait en ligne. Un navigateur suffit.

mon-oral.net est multiplateforme, il fonctionne sur ordinateur, téléphone ou tablette.

Cet atelier proposera:
* une présentation générale de mon-oral.net (historique, fonctionnalités, utilisation...)
* une démonstration de création d'une activité
* une démonstration de création d'un entraînement pour l'épreuve anticipée de français et le Grand oral
* une présentation de l'environnement de développement collaboratif"