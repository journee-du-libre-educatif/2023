---
name: Olivier Adam
first_name: Olivier
last_name: Adam
---

Ex-enseignant de sciences et technique industrielle, passionné d'informatique, contributeur au libre depuis 20 ans par la mise à disposition d'ESU pour le serveur EOLE-SCRIBE, et par la mise en œuvre de Toutatice, aujourd'hui en étant DSI de l'académie de Rennes.