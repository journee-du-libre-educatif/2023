---
name: Wikimédia France + Vikidia
last_name: Wikimédia France + Vikidia
pluriel: true
---

Une présentation par Mathilde Louis et Jérôme Hublart.

Wikimédia France et le CLEMI proposent l’animation d’un atelier d’initiation à Vikidia, l’encyclopédie des ados.

Au cours de cet atelier, nous ferons une introduction générale de [Vikidia](https://fr.vikidia.org/wiki/Vikidia:Accueil) et verrons les plus-values de ce wiki pour la mise en place de projets pédagogiques, notamment pour l'éducation critique aux médias et à l'information (ECMI).

Nous verrons comment [Vikidia](https://fr.vikidia.org/wiki/Vikidia:Accueil) se distingue de Wikipédia et pourquoi elle est bien plus facile d’utilisation que sa « grande sœur ». Un temps de pratique sera proposé pour que les participants puissent mettre la main à la pâte et _a minima_ se créer un compte utilisateur et naviguer sur le site.