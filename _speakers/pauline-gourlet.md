---
name: Pauline Gourlet
first_name: Pauline
last_name: Gourlet
ajout_e: true
---

Pauline Gourlet fait partie de _L'Atelier des chercheurs_, un collectif de trois designers qui conçoit des outils libres de documentation et d'écriture collaborative depuis 2012.