---
name: Julien Bonhomme
first_name: Julien
last_name: Bonhomme
---

Médiateur numérique, en charge du [Lab de l'Atelier Canopé 57](https://www.reseau-canope.fr/academie-de-nancy-metz/atelier-canope-57-montigny-les-metz/lilab.html/), du suivi des projets Fablab à l'école et du [Festival du film scolaire en Moselle](https://atelier-canope57.canoprof.fr/eleve/FestivalFilmScolaire/).

Utilisateur et contributeur au développement de [do•doc](https://latelier-des-chercheurs.fr/outils/dodoc)