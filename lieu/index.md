---
layout: location
title: Localisation
---

Le [Couvent des Jacobins](https://www.centre-congres-rennes.fr/) se situe 20 place Sainte-Anne en plein centre ville de Rennes, à la sortie de la station de métro Saint-Anne.

C'est à 10 min de la gare en métro (possible de payer sans contact avec sa carte bancaire). <br>Depuis la gare (station Gares), deux possibilités, un même arrêt ("Sainte Anne") :
- Soit ligne A direction J.F. Kennedy (à 3 stations de la gare)
- Soit ligne B direction Cesson Viasilva (à 2 stations de la gare)

{% if site.conference.location.map %}
  <div id="map" class="mt-5 mb-5"></div>
{% endif %}

Le stationnement en centre ville Rennes est compliqué et onéreux. Si vous venez en voiture, il est conseillé d'utiliser les parcs relais en périphérie et de poursuivre en transport en commun. Pour identifier ces parcs relais : [réseau STAR](https://www.star.fr/se-deplacer/parc-relais/carte-des-parcs-relais), parking libre et gratuit sur présentation d'un titre de transport (métro ou bus) validé.

Les personnels de l'académie de Nantes pourront utiliser le service covoiturage intégré à l'application SOFIA FMO.